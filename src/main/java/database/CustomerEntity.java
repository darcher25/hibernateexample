package database;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "customer", schema = "customers", catalog = "")
public class CustomerEntity {
    private int customerNumber;
    private String customerFirstName;
    private String customerLastName;
    private String phone;
    private String addressLine;
    private String city;
    private String state;
    private String postalCode;
    private Collection<OrdersEntity> ordersByCustomerNumber;

    @Id
    @Column(name = "customerNumber")
    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    @Basic
    @Column(name = "customerFirstName")
    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    @Basic
    @Column(name = "customerLastName")
    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "addressLine")
    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "postalCode")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerEntity that = (CustomerEntity) o;

        if (customerNumber != that.customerNumber) return false;
        if (customerFirstName != null ? !customerFirstName.equals(that.customerFirstName) : that.customerFirstName != null)
            return false;
        if (customerLastName != null ? !customerLastName.equals(that.customerLastName) : that.customerLastName != null)
            return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (addressLine != null ? !addressLine.equals(that.addressLine) : that.addressLine != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (postalCode != null ? !postalCode.equals(that.postalCode) : that.postalCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = customerNumber;
        result = 31 * result + (customerFirstName != null ? customerFirstName.hashCode() : 0);
        result = 31 * result + (customerLastName != null ? customerLastName.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (addressLine != null ? addressLine.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (postalCode != null ? postalCode.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "customerByCustomerNumber")
    public Collection<OrdersEntity> getOrdersByCustomerNumber() {
        return ordersByCustomerNumber;
    }

    public void setOrdersByCustomerNumber(Collection<OrdersEntity> ordersByCustomerNumber) {
        this.ordersByCustomerNumber = ordersByCustomerNumber;
    }
}
