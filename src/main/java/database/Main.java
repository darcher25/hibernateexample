package database;

import database.CustomerEntity;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {
        TestDAO t = TestDAO.getInstance();

        List<CustomerEntity> c = t.getCustomers();
        for (CustomerEntity i : c) {
            System.out.println(i);
        }

        System.out.println(t.getCustomer(1));


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();

            CustomerEntity carmen = new CustomerEntity();
            carmen.setCustomerFirstName("Carmen");
            carmen.setCustomerLastName("Barrett");
            carmen.setPhone("5552614418");
            carmen.setAddressLine("100 Stone Dr");
            carmen.setCity("Manchester");
            carmen.setState("TN");
            carmen.setPostalCode("37355");
            //make sure you use the use the persist method
            //otherwise your object won't be persistent
            //inside the parenthesis is the variable not a string

            entityManager.persist(carmen);
            transaction.commit();

        }finally {
            if (transaction.isActive()){
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
       /* try {
            transaction.begin();

            OrdersEntity book = new OrdersEntity();
            book.setOrderDate("20220303");
            book.setComments("Add extra bubble wrap around book for shipping.");

            //make sure you use the use the persist method
            //otherwise your object won't be persistent
            //inside the parenthesis is the variable not a string

            entityManager.persist(book);
            transaction.commit();

        }finally {
            if (transaction.isActive()){
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }

        */
    }
}
